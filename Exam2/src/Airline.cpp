#include "Airline.h"

Airline::Airline(string airlineName) {
	this->airlineName = airlineName;
}

void Airline::addFlight(Flight* newFlight){
	flights.push_back(*newFlight);
}

void Airline::addPassenger(Passenger* newPassenger){
	passengers.push_back(*newPassenger);
}

void Airline::addPilot(Pilot* newPilot){
	pilots.push_back(*newPilot);
}

bool Airline::removePassengerByName(string name){
	for(unsigned i = 0; i <passengers.size(); ++i){
		if(name.compare(passengers[i].getName()) == 0){
			passengers.erase(passengers.begin()+i);
			return true;
		}
	}
	return false;
}

bool Airline::removePilotByName(string name){
	for(unsigned i = 0; i <pilots.size(); ++i){
		if(name.compare(pilots[i].getName()) == 0){
			pilots.erase(pilots.begin()+i);
			return true;
		}
	}
	return false;
}

bool Airline::removeFlightByNumber(string number){
	for(unsigned i = 0; i <flights.size(); ++i){
		if(number == (flights.at(i).getNumber())){
			flights.erase(flights.begin()+i);
			return true;
		}
	}
	return false;
}


void Airline::addPassengerToFlight(Flight* aFlight, Passenger* aPassenger){
	if (aPassenger == NULL)
		return;
	vector<Flight>::iterator it = flights.begin();
		while (it != flights.end()){
			if((*it).getNumber() == (*aFlight).getNumber()){
				(&(*it))->addPassenger(aPassenger);
			}
			++it;
	}
}

void Airline::removePassengerFromFlight(Flight* aFlight, Passenger* aPassenger){
	if (aPassenger == NULL)
		return;
	vector<Flight>::iterator it = flights.begin();
		while (it != flights.end()){
			if((*it).getNumber() == (*aFlight).getNumber()){
				(&(*it))->removePassenger(aPassenger);
			}
			++it;
	}
}

void Airline::addPilotToFlight(Flight* aFlight, Pilot* aPilot){
	if (aPilot == NULL)
		return;
	vector<Flight>::iterator it = flights.begin();
		while (it != flights.end()){
			if((*it).getNumber() == (*aFlight).getNumber()){
				(&(*it))->addPilot(aPilot);
			}
			++it;
	}
}

void Airline::removePilotFromFlight(Flight* aFlight, Pilot* aPilot){
	if (aPilot == NULL)
		return;
	vector<Flight>::iterator it = flights.begin();
		while (it != flights.end()){
			if((*it).getNumber() == (*aFlight).getNumber()){
				(&(*it))->removePilot(aPilot);
			}
			++it;
	}
}

Passenger* Airline::getPassengerByName(string name){
	vector<Passenger>::iterator it = passengers.begin();
	while (it != passengers.end()){
		if((*it).getName() == name){
			return (&(*it));
		}
		++it;
	}
	return NULL;
}

Pilot* Airline::getPilotByName(string name){
	vector<Pilot>::iterator it = pilots.begin();
	while (it != pilots.end()){
		if((*it).getName() == name){
			return (&(*it));
		}
		++it;
	}
	return NULL;
}

Flight* Airline::getFlightByNumber(string number){
	vector<Flight>::iterator it = flights.begin();
	while (it != flights.end()){
		if((*it).getNumber() == number){
			return (&(*it));
		}
		++it;
	}
	return NULL;
}

