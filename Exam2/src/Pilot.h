#ifndef PILOT_H_
#define PILOT_H_
#include "Person.h"

class Pilot: public Person {
private:
	string licenseNumber;

public:
	Pilot();
	Pilot(string name, string licenseNumber);
	string getLicenseNumber();
	string getName();
};

#endif /* PILOT_H_ */
