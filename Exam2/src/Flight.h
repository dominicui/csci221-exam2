#ifndef FLIGHT_H_
#define FLIGHT_H_
#include <string>
#include <vector>
#include "Passenger.h"
#include "Pilot.h"

using namespace std;

class Flight {
private:
	string number;
	pair<string, string> route;
	vector<Pilot>* pilots;
	vector<Passenger>* passengers;

public:
	Flight(string,string,string);
	string getFrom();
	void setFrom(string from);
	string getNumber();
	void setNumber(string number);
	string getTo();
	void setTo(string to);
	vector<Pilot> getPilots();
	vector<Passenger> getPassengers();
	void addPilot(Pilot* aPilot);
	void addPassenger(Passenger* aPassenger);
	bool removePilot(Pilot* aPilot);                //TODO:
	bool removePassenger(Passenger* aPassenger);    //TODO:
};



#endif /* FLIGHT_H_ */
