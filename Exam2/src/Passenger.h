#ifndef PASSENGER_H_
#define PASSENGER_H_

#include <vector>
#include <string>
#include "Person.h"
using namespace std;

class Passenger : public Person {
private:
	string passportNumber;

public:
	Passenger();
	Passenger(string name, string passportNumber);
	string getName();
	string getPassportNumber();
};

#endif /* PASSENGER_H_ */
