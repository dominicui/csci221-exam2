#include "Passenger.h"

Passenger::Passenger(){

}

Passenger::Passenger(string name, string passportNumber){
	this->name = name;
	this->passportNumber = passportNumber;
}

string Passenger::getName(){
	return this->name;
}

string Passenger::getPassportNumber(){
	return this->passportNumber;
}
