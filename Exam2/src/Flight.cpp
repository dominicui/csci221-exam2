#include "Flight.h"
#include <algorithm>

Flight::Flight(string number, string from, string to){
	this->number = number;
	this->route = make_pair(from, to);
	this->passengers = new vector<Passenger>();
	this->pilots = new vector<Pilot>();
}

string Flight::getFrom() {
	return this->route.first;
}

void Flight::setFrom(string from) {
	this->route.first = from;
}

string Flight::getNumber() {
	return number;
}

void Flight::setNumber(string number) {
	this->number = number;
}

string Flight::getTo() {
	return this->route.second;
}

void Flight::setTo(string to) {
	this->route.second = to;
}

vector<Pilot> Flight::getPilots(){
	return *pilots;
}

vector<Passenger> Flight::getPassengers(){
	return *passengers;
}

void Flight::addPilot(Pilot* aPilot){
	pilots->push_back(*aPilot);
}

void Flight::addPassenger(Passenger* aPassenger){
	passengers->push_back(*aPassenger);
}

bool Flight::removePilot(Pilot* aPilot){
	for(unsigned i = 0; i <pilots->size(); ++i){
		if(aPilot->getName() == pilots->at(i).getName()){
			pilots->erase(pilots->begin()+i);
			return true;
		}
	}
	return false;
}

bool Flight::removePassenger(Passenger* aPassenger){
	for(unsigned i = 0; i <passengers->size(); ++i){
		if(aPassenger->getName() == passengers->at(i).getName()){
			passengers->erase(passengers->begin()+i);
			return true;
		}
	}
	return false;
}
