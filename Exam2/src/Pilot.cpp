#include "Pilot.h"

Pilot::Pilot() {

}

Pilot::Pilot(string name, string licenseNumber){
	this->name = name;
	this->licenseNumber = licenseNumber;
}

string Pilot::getName(){
	return this->name;
}

string Pilot::getLicenseNumber(){
	return this->licenseNumber;
}
