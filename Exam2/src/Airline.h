#ifndef AIRLINE_H_
#define AIRLINE_H_
#include <iostream>

#include <vector>
#include <utility>
#include <string>
#include "Passenger.h"
#include "Pilot.h"
#include "Flight.h"
using namespace std;

class Airline {

private:
	string airlineName;
	vector<Passenger> passengers;
	vector<Pilot> pilots;
	vector<Flight> flights;

public:
	Airline(string);

	Passenger* getPassengerByName(string name);
	Pilot* getPilotByName(string name);
	Flight* getFlightByNumber(string number);

	void addPassenger(Passenger* newPassenger);
	void addPilot(Pilot* newPilot);
	void addFlight(Flight* newFlight);

	bool removePassengerByName(string name);
	bool removePilotByName(string name);                                 //TODO:
	bool removeFlightByNumber(string number);                            //TODO:

	void addPassengerToFlight(Flight* aFlight, Passenger* aPassenger);
	void addPilotToFlight(Flight* aFLight, Pilot* aPilot);                    //TODO:
	void removePassengerFromFlight(Flight* aFlight, Passenger* aPassenger);   //TODO:
	void removePilotFromFlight(Flight* aFLight, Pilot* aPilot);               //TODO:
};

#endif /* AIRLINE_H_ */
