/*
Airline.o: Airline.cpp Airline.h Passenger.h Pilot.h Flight.h
	g++ -Wall -ansi -pedantic -g -O2 -c Airline.cpp

Flight.o: Flight.cpp Flight.h Passenger.h Pilot.h
	g++ -Wall -ansi -pedantic -g -O2 -c Flight.cpp

Passenger.o: Passenger.cpp Passenger.h Person.h
	g++ -Wall -ansi -pedantic -g -O2 -c Passenger.cpp

Person.o: Person.cpp Person.h
	g++ -Wall -ansi -pedantic -g -O2 -c Person.cpp

Pilot.o: Pilot.cpp Pilot.h Person.h
	g++ -Wall -ansi -pedantic -g -O2 -c Pilot.cpp

Main.o: Main.cpp Airline.h Flight.h Passenger.h Pilot.h
	g++ -Wall -ansi -pedantic -g -O2 -c Main.cpp

Exam2: Airline.o Flight.o Passenger.o Person.o Pilot.o Main.o
	g++ -Wall -ansi -pedantic -g -O2 -o Exam2 Airline.o Flight.o Passenger.o Person.o Pilot.o Main.o
 */

#include <iostream>
#include <iomanip>

#include "Airline.h"
#include "Flight.h"
#include "Passenger.h"
#include "Pilot.h"

using namespace std;

int main() {

	Airline *myAirline = new Airline("StetsonAirlines");

	//Create 8 passengers
	myAirline->addPassenger(new Passenger("James Smith", "P38313"));
	myAirline->addPassenger(new Passenger("Michael Smith", "P34810"));
	myAirline->addPassenger(new Passenger("Robert Smith", "P34269"));
	myAirline->addPassenger(new Passenger("Maria Garcia", "P32092"));
	myAirline->addPassenger(new Passenger("David Smith", "P31294"));
	myAirline->addPassenger(new Passenger("Maria Rodriguez", "P30507"));
	myAirline->addPassenger(new Passenger("Mary Smith", "P28692"));
	myAirline->addPassenger(new Passenger("Maria Hernandez", "P27836"));

	//Create 6 pilots
	myAirline->addPilot(new Pilot("James H. Doolittle", "001"));
	myAirline->addPilot(new Pilot("Noel Wien", "002"));
	myAirline->addPilot(new Pilot("Robert A. Hoover", "003"));
	myAirline->addPilot(new Pilot("Charles A. Lindbergh", "004"));
	myAirline->addPilot(new Pilot("Charles E. Yeager", "005"));
	myAirline->addPilot(new Pilot("Scott Crossfield", "006"));

	//Create 6 empty flights (with 0 passenger, 0 pilot)
	myAirline->addFlight(new Flight("SA101","DeLand","Orlando"));
	myAirline->addFlight(new Flight("SA102","DeLand","Jacksonville"));
	myAirline->addFlight(new Flight("SA103","DeLand","Tampa"));
	myAirline->addFlight(new Flight("SA104","DeLand","Miami"));
	myAirline->addFlight(new Flight("SA105","DeLand","New York"));
	myAirline->addFlight(new Flight("SA106","DeLand","Chicago"));

	//Remove a passenger by name
	myAirline->removePassengerByName("Maria Hernandez");

	//TODO***
	//Remove a pilot by name
	myAirline->removePilotByName("James H. Doolittle");

	//TODO***
	//Remove a flight by number
	myAirline->removeFlightByNumber("SA104");

	//Add a passenger with name to a flight manifest with flight number. (If passenger is not registered in the airline system, it fails!)
	myAirline->addPassengerToFlight(myAirline->getFlightByNumber("SA101"), myAirline->getPassengerByName("Maria Hernandez")); // should not be added
	myAirline->addPassengerToFlight(myAirline->getFlightByNumber("SA101"), myAirline->getPassengerByName("Mary Smith"));
	myAirline->addPassengerToFlight(myAirline->getFlightByNumber("SA101"), myAirline->getPassengerByName("Michael Smith"));
	myAirline->addPassengerToFlight(myAirline->getFlightByNumber("SA104"), myAirline->getPassengerByName("James Smith"));

	//TODO***
	//Add a pilot with name to a flight manifest with flight number. (If pilot is not registered in the airline system, it fails!)
	myAirline->addPilotToFlight(myAirline->getFlightByNumber("SA101"), myAirline->getPilotByName("James Smith"));
	myAirline->addPilotToFlight(myAirline->getFlightByNumber("SA101"), myAirline->getPilotByName("Charles A. Lindbergh"));
	myAirline->addPilotToFlight(myAirline->getFlightByNumber("SA101"), myAirline->getPilotByName("James H. Doolittle"));
	myAirline->addPilotToFlight(myAirline->getFlightByNumber("SA101"), myAirline->getPilotByName("Robert A. Hoover"));

	//TODO***
	//Remove a passenger with name from a flight manifest with flight number. (If passenger is not registered in the airline system, it fails!)
	myAirline->removePassengerFromFlight(myAirline->getFlightByNumber("SA101"), myAirline->getPassengerByName("James Smith"));

	//TODO***
	//Remove a pilot with name from a flight manifest with flight number. (If pilot is not registered in the airline system, it fails!)
	myAirline->removePilotFromFlight(myAirline->getFlightByNumber("SA101"), myAirline->getPilotByName("Charles A. Lindbergh"));


	//Get a flight manifest to test the program.
	vector<Passenger> flightManifest = (myAirline->getFlightByNumber("SA101"))->getPassengers();
	cout<<"Flight: SA101 passengers"<<endl;
	for(unsigned i = 0; i < flightManifest.size(); ++i){
		cout << (i+1) << ". " << setw(6) << flightManifest[i].getPassportNumber() << setw(16) << flightManifest[i].getName() << endl;
	}
	cout<<endl;

	vector<Passenger> flightManifest2 = (myAirline->getFlightByNumber("SA104"))->getPassengers();
	cout<<"Flight: SA104 passengers"<<endl;
	for(unsigned i = 0; i < flightManifest2.size(); ++i){
		cout << (i+1) << ". " << setw(6) << flightManifest2[i].getPassportNumber() << setw(16) << flightManifest2[i].getName() << endl;
	}
	cout<<endl;

	vector<Pilot> flightManifest3 = (myAirline->getFlightByNumber("SA101"))->getPilots();
	cout<<"Flight: SA101 pilots"<<endl;
	for(unsigned i = 0; i < flightManifest3.size(); ++i){
		cout << (i+1) << ". " << setw(6) << flightManifest3[i].getLicenseNumber() << setw(16) << flightManifest3[i].getName() << endl;
	}
	cout<<endl;

	cout << myAirline->getFlightByNumber("SA102") << endl;
	//End of test.

	return 0;
}
